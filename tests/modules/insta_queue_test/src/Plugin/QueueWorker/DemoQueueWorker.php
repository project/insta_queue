<?php

declare(strict_types=1);

namespace Drupal\insta_queue_test\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\Attribute\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\insta_queue_test\CounterService;
use Psr\Container\ContainerInterface;

/**
 * Dispatch cache webhooks.
 */
#[QueueWorker(
  id: 'demo_queue_worker',
  title: new TranslatableMarkup('Demo Queue Worker'),
  cron: ['time' => 30],
)]
class DemoQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  const QUEUE_NAME = 'demo';

  /**
   * Construct new instance.
   */
  public function __construct(
    protected CounterService $counter,
    array $configuration,
    string $plugin_id,
    mixed $plugin_definition,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): self {
    return new static(
      $container->get(CounterService::class),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->counter->increment();
  }

}
