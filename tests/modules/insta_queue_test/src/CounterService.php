<?php

declare(strict_types=1);

namespace Drupal\insta_queue_test;

/**
 * Simple service to count calls.
 */
class CounterService {

  /**
   * Counter.
   */
  public static int $counter = 0;

  /**
   * Increment counter.
   */
  public function increment() {
    self::$counter++;
  }

  /**
   * Get counter.
   */
  public function getCounter() {
    return self::$counter;
  }

}
