<?php

declare(strict_types=1);

namespace Drupal\Tests\insta_queue\Kernel;

use Drupal\insta_queue\Exception\SchedulerConnectionMisconfiguredException;
use Drupal\insta_queue\Queue\InstaQueueInterface;
use Drupal\insta_queue\Scheduler\SchedulerClient;
use Drupal\insta_queue\Scheduler\SchedulerClientInterface;
use Drupal\insta_queue_test\CounterService;
use Drupal\KernelTests\KernelTestBase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Tests the InstaQueue module.
 *
 * @group insta_queue
 */
class InstaQueueTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'insta_queue',
    'insta_queue_test',
  ];

  /**
   * Test that the demo queue worker is an insta queue.
   */
  public function testInstaQueueWorker(): void {
    // Make the queue worker a insta queue.
    $this->setSetting('queue_service_demo_queue_worker', 'queue.insta_queue.database');

    $queue = \Drupal::queue('demo_queue_worker');
    $this->assertInstanceOf(InstaQueueInterface::class, $queue);
  }

  /**
   * Test that an exception is thrown when connection is not configured.
   */
  public function testSchedulerClientNotConfigured(): void {
    $client = \Drupal::service('insta_queue.scheduler_client');

    $this->expectException(SchedulerConnectionMisconfiguredException::class);
    $client->checkConnection();
  }

  /**
   * Test that an exception is thrown when connection is misconfigured.
   */
  public function testSchedulerClientMissingTimeoutConfig(): void {
    $client = \Drupal::service('insta_queue.scheduler_client');

    $this->setSetting('insta_queue.scheduler_connection', [
      'address' => 'tcp://127.0.0.1:5555',
    ]);

    $this->expectException(SchedulerConnectionMisconfiguredException::class);
    $client->checkConnection();
  }

  /**
   * Test that an exception is thrown when address is not tcp:// or unix://.
   */
  public function testSchedulerClientWrongProtocol(): void {
    $client = \Drupal::service('insta_queue.scheduler_client');

    $this->setSetting('insta_queue.scheduler_connection', [
      'address' => 'https://127.0.0.1:5555',
      'timeout' => 10,
    ]);

    $this->expectException(SchedulerConnectionMisconfiguredException::class);
    $client->checkConnection();
  }

  /**
   * Test working scheduler client.
   */
  public function testConfiguredScheduler(): void {
    $client = \Drupal::service('insta_queue.scheduler_client');

    // Accept connection.
    $sock = socket_create_listen(85324);
    socket_getsockname($sock, $addr, $port);

    $this->setSetting('insta_queue.scheduler_connection', [
      'address' => 'tcp://127.0.0.1:' . $port,
      'timeout' => 10,
    ]);

    $this->assertTrue($client->checkConnection());

    socket_close($sock);
  }

  /**
   * Tests the rebuild hook.
   */
  public function testCacheRebuild() {
    $mock = $this->createSchedulerMock();

    $mock->expects($this->once())
      ->method('notifyReload');

    // This hook is normally called by a cache rebuild.
    insta_queue_rebuild();
  }

  /**
   * Tests the queue item creation.
   */
  public function testQueueItemCreated() {
    // Make the queue worker a insta queue.
    $this->setSetting('queue_service_demo_queue_worker', 'queue.insta_queue.database');

    $mock = $this->createSchedulerMock();

    $mock->expects($this->once())
      ->method('notifyQueueItemCreated')
      ->with('demo_queue_worker');

    $queue = \Drupal::queue('demo_queue_worker');
    $queue->createItem(['data' => 'test']);
  }

  /**
   * Test the queue processor.
   */
  public function testQueueProcessor() {
    // Make the queue worker a insta queue.
    $this->setSetting('queue_service_demo_queue_worker', 'queue.insta_queue.database');
    $this->createSchedulerMock();

    $queue = \Drupal::queue('demo_queue_worker');
    $queue->createItem(['data' => 'test']);

    $counter = $this->container->get(CounterService::class);

    $this->assertEquals(0, $counter->getCounter());
    \Drupal::service('insta_queue.processor')->processQueue('demo_queue_worker');
    $this->assertEquals(1, $counter->getCounter());

    // Add 3 more.
    $queue->createItem(['data' => 'test']);
    $queue->createItem(['data' => 'test']);
    $queue->createItem(['data' => 'test']);

    \Drupal::service('insta_queue.processor')->processQueue('demo_queue_worker');
    $this->assertEquals(4, $counter->getCounter());
  }

  /**
   * Create a mock scheduler client.
   */
  protected function createSchedulerMock(): MockObject {
    $mock = $this->createMock(SchedulerClientInterface::class);
    $this->container->set(SchedulerClient::class, $mock);

    return $mock;
  }

}
