<?php

declare(strict_types=1);

namespace Drupal\Tests\insta_queue\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Test insta_queue module install.
 */
class InstaQueueInstallTest extends EntityKernelTestBase {

  /**
   * Test that module can be installed.
   */
  public function testModuleInstall(): void {
    $this->installModule('insta_queue');

    $this->assertTrue(TRUE);
  }

}
