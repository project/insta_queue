# Drupal Account Portal

## Contents of this file

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Introduction

This module provides the fundamental functionality to implement an
"Account Portal", like how Google does it when you log in to a website
with your Google account (accounts.google.com).

### For what is this for?

When you want to build a "portal" to provide account functionality
for your OAuth applications, like login, registration, etc.

The idea is to create a contained area in drupal where the user get's redirected
when loging in. This is mainly the `/oauth/authorize` endpoint from
Simple OAuth. Depending in your setup, the user can then login or register,
handle 2FA or even Passkeys.

You might also be interested in the [Simple OAuth Account Picker](https://www.drupal.org/project/simple_oauth_account_picker)
module that let's the user pick the current account or previous accounts,
just like Google does it.

### Functionality

The following functionality is provided:

- Deeply integrates with the `consumers` module to distinguish requests from
  different consumers.
- Each request is expected to be prefixed with an account portal path and the
  consumer id. e.g. `/account-portal/realm/_consumer-id_/user/login`.
- Configurable routes which will have the base-path prefixed when the URL to
  that route is generated.
- Provides a utility to get the origin from where the user comes from.

Various utility functions are provided in `AccountPortalUtility`.

Most notably the `AccountPortalUtility::getRedirectUri()` method is
used to get the redirect URL to where the user came from.
The URL is either infered from the `redirect_uri` GET parameter, which
is set when using the **Authorization Code Grant** or the
`Referer` HTTP header.

### Why not use the core routes?

One coud ask why not just use the core routes, like `/user/login`.
This indeed would work, but the problem is that in more complex flows,
like user registration, the user needs to go through multiple steps
(pages) before being redirected back to the external application.

If the user wants to abort the process and go back to the external
application, it would be very hard to get the information from where
the user came from.

Therefore by redirecting the user to an account portal route, the needed
information is always available.

This would also enable you to customize the account portal pages on a
per consumer basis. (e.g. show custom links, if the user comes from
a native mobile app, etc.)

## Requirements

The core module requires Drupal 10 and the following contrib modules:

- [Consumers](https://www.drupal.org/project/consumers)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configuration is done via service paramters (add in your `services.yaml`):

```yaml
parameters:
  # The prefix for account portal routes.
  # E.g. /account-portal/realm/_consumer-id_/user/login
  account_portal.base_path: '/account-portal/realm'

  # If a account portal route is requested with an invalid
  # consumer id, redirect to the following route.
  # the default consumer is used instead.
  # This can be null, where no redirection will happen and
  account_portal.invalid_consumer_id_destination: '<front>'

  # Define all routes that shall be supported by the account portal.
  # If a URL to one of the following routes is being generated on
  # a page accessed via the account portal, the URL will have the
  # account portal path prefixed.
  account_portal.routes:
    - 'user.login'
    - 'user.pass'
    - 'user.register'
```

## Maintainers

Current maintainers:

- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))








# Drupal Insta Queue

## Contents of this file

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Introduction

This modules provides support for instant, real-time queues!
**No more waiting on the next cron run to get queues processed!**

The module does not work on its own and requires the [Insta Queue Scheduler](https://www.drupal.org/project/insta_queue_scheduler)
program running alongside PHP on your server.

By not altering Drupal Core's queue implementation, this module can be used
with any queues, be it drupal core ones or from third party modules.

### Functionality

The following functionality is provided:

- Allows queues to be configured to use the `queue.insta_queue.database` service.
- Does not alter the core queue implementation.
- Uses the core database queue implementation under the hood.
- Works with any queues, be it drupal core ones or from third party modules.
- If new items are added to the queue while processing the queue will continue
  processing the queue until the queue is empty.
- Processing timeout makes sure that a single drush command is restarted after
  said timeout to avoid memory leaks.
- Supports the [Queue UI](https://www.drupal.org/project/queue_ui) module.
- Processes multiple insta queues in parallel.

### How does it work?

Each queue can be configured to use the `queue.insta_queue.database` service
for the queue implementation.

This service wraps the core `queue.database` service but communicates with
the scheduler to notify it whenever a queue item is added to the queue.

The scheduler than immediately starts processing the queue that got the
item added to it.

Additionally, the scheduler will also periodically check the queues for
items that need to be processed. This is the same behaviour as when running
the queue via cron.

A queue using the `queue.insta_queue.database` service will not be processed
by the core queue cron!

#### Processing the queue

A custom drush command is provided to process a particular queue that is
configured to use the `queue.insta_queue.database` service.

All that the scheduler program does is call the drush command as a seaprate
process and waits until it is completed.

This enables the scheduler to process each queue in parallel by spawning
a separate process for each queue.

## Requirements

The core module requires Drupal 10 or 11 and the following composer dependencies:

- [Drush](https://www.drush.org/13.x/)

**Additionally, the Insta Queue Scheduler program is required!**

See here to download prebuilt binaries: [https://www.drupal.org/project/insta_queue_scheduler/issues/3403219](https://www.drupal.org/project/insta_queue_scheduler/issues/3403219)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

The scheduler program needs to be setup in a way that it can:
- Run as a service.
- Be able to execute the `drush insta_queue:process-queue` command.
- Communicate with this module via TCP or a unix socket.

## Configuration

Configuration is via service parameters and drupal settings:

```yaml
parameters:
  # Maximum time in seconds to work on processing a queue.
  insta_queue.process_max_time: 60
```

```php
// The socket is created by the scheduler program.
// Must be accessable by PHP.
$settings['insta_queue.scheduler_connection']['address'] = 'unix:///run/iqs.sock';

// Alternatively, TCP can be used.
// $settings['insta_queue.scheduler_connection']['address'] = 'tcp://127.0.0.1:1234';

// Maximum time in seconds to wait for the socket or TCP connection.
// Should be set relatively low to avoid blocking operations.
$settings['insta_queue.scheduler_connection']['timeout'] = 5;
```

## Maintainers

Current maintainers:

- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))
