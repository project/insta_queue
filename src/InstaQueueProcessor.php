<?php

declare(strict_types=1);

namespace Drupal\insta_queue;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Queue\DelayableQueueInterface;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Core\Queue\QueueWorkerInterface;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\Core\Utility\Error;
use Drupal\insta_queue\Exception\InvalidQueueException;
use Drupal\insta_queue\Queue\InstaQueueInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Service to process a given queue.
 *
 * This is largely taken from Drupal's core Cron.php implementation,
 * but altered to only process one given queue.
 */
class InstaQueueProcessor implements InstaQueueProcessorInterface {

  /**
   * Construct a new QueueDispatcher.
   */
  public function __construct(
    protected QueueFactory $queueFactory,
    protected QueueWorkerManagerInterface $queueManager,
    protected TimeInterface $time,
    #[Autowire(service: 'lock')]
    protected LockBackendInterface $lock,
    #[Autowire(service: 'logger.channel.insta_queue')]
    protected LoggerInterface $logger,
    #[Autowire(param: 'queue.config')]
    protected array $queueConfig,
    #[Autowire(param: 'insta_queue.process_max_time')]
    protected int $processMaxTime,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function processQueue(string $queueName): QueueProcessResult {
    $lockName = 'queue_processor:' . $queueName;

    if (!$this->lock->acquire($lockName, $this->processMaxTime)) {
      $this->logger->warning('Attempting to process queue "@queue" while it is already processing!', [
        '@queue' => $queueName,
      ]);

      $result = QueueProcessResult::ALREADY_RUNNING;
    }
    else {
      $queue = $this->queueFactory->get($queueName);

      // Only handle insta queues.
      if ($queue instanceof InstaQueueInterface === FALSE) {
        throw new InvalidQueueException($queueName);
      }

      // Make sure every queue exists. There is no harm in trying to recreate
      // an existing queue.
      $queue->createQueue();
      $worker = $this->queueManager->createInstance($queueName);

      $result = $this->doProcessQueue($queue, $worker);

      // Release lock.
      $this->lock->release($lockName);
    }

    return $result;
  }

  /**
   * Process given queue with the given worker.
   *
   * The code is mostly taken from the core cron implementation.
   *
   * The following is different:
   * - A service parameter defines the max time for the queue process.
   *   This takes precedence over the queue cron time limit, if the
   *   cron time limit is larger than the service parameter.
   * - The method returns a dedicated result, depending on the
   *   queue execution.
   *
   * @returns QueueProcessResult
   *   The process result. Enum value corresponds to process exit code.
   */
  protected function doProcessQueue(QueueInterface $queue, QueueWorkerInterface $worker): QueueProcessResult {
    $leaseTime = $worker->getPluginDefinition()['insta_queue']['worker_max_process_time'];
    $maxTime = $leaseTime > $this->processMaxTime ? $this->processMaxTime : $leaseTime;

    $end = $this->time->getCurrentTime() + $maxTime;
    $lastProcessStart = $this->time->getCurrentTime();

    // Try to claim every possible item within the time limit.
    while ($this->time->getCurrentTime() < $end && ($item = $queue->claimItem($maxTime))) {
      $lastProcessStart = $this->time->getCurrentTime();

      try {
        // @phpstan-ignore property.nonObject
        $worker->processItem($item->data);
        $queue->deleteItem($item);
      }
      catch (DelayedRequeueException $e) {
        // The worker requested the task not be immediately re-queued.
        // - If the queue doesn't support ::delayItem(), we should leave the
        // item's current expiry time alone.
        // - If the queue does support ::delayItem(), we should allow the
        // queue to update the item's expiry using the requested delay.
        if ($queue instanceof DelayableQueueInterface) {
          // This queue can handle a custom delay; use the duration provided
          // by the exception.
          $queue->delayItem($item, $e->getDelay());
        }
      }
      catch (RequeueException) {
        // The worker requested the task be immediately requeued.
        $queue->releaseItem($item);
      }
      catch (SuspendQueueException $e) {
        // If the worker indicates the whole queue should be skipped, release
        // the item and go to the next queue.
        $queue->releaseItem($item);

        $this->logger->debug('A worker for @queue queue suspended further processing of the queue.', [
          '@queue' => $worker->getPluginId(),
        ]);

        return QueueProcessResult::QUEUE_SUSPENDED;
      }
      catch (\Exception $e) {
        // In case of any other kind of exception, log it and leave the item
        // in the queue to be processed again later.
        Error::logException($this->logger, $e);
      }
    }

    // If the process has ended due to timelimit.
    if ($lastProcessStart >= $end) {
      return QueueProcessResult::TIME_LIMIT_EXCEEDED;
    }

    // Otherwise items are processed.
    return QueueProcessResult::AVAILABLE_ITEMS_PROCESSED;
  }

}
