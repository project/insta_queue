<?php

declare(strict_types=1);

namespace Drupal\insta_queue\EventSubscriber;

use Drupal\insta_queue\Event\InstaQueueItemCreatedEvent;
use Drupal\insta_queue\InstaQueueEvents;
use Drupal\insta_queue\Scheduler\SchedulerClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribe to insta queue events.
 */
class InstaQueueEventSubscriber implements EventSubscriberInterface {

  /**
   * Construct new InstaQueueEventSubscriber.
   */
  public function __construct(
    protected readonly SchedulerClientInterface $schedulerClient,
    #[Autowire(service: 'logger.channel.insta_queue')]
    protected readonly LoggerInterface $logger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      InstaQueueEvents::QUEUE_ITEM_CREATED => [
        ['handleItemCreated'],
      ],
    ];
  }

  /**
   * Handle insta queue item created event.
   */
  public function handleItemCreated(InstaQueueItemCreatedEvent $event): void {
    try {
      $this->schedulerClient->notifyQueueItemCreated($event->getQueueName());
    }
    catch (\Exception $e) {
      $this->logger->critical('Error notifying insta_queue scheduler: @error', [
        '@error' => $e->getMessage(),
      ]);
    }
  }

}
