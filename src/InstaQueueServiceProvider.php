<?php

declare(strict_types=1);

namespace Drupal\insta_queue;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\insta_queue\Queue\InstaQueueWorkerManager;

/**
 * Service provider for insta_queue module.
 */
class InstaQueueServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    $this->conditionallyDecorateQueueWorkerManager($container);
  }

  /**
   * Conditinally decorates the core QueueWorkerManager service.
   *
   * If the service parameter `insta_queue.decorate_queue_worker_manager` is set
   * to true, than the core plugin.manager.queue_worker service is decorated by
   * the plugin.manager.insta_queue_worker service from this module.
   */
  protected function conditionallyDecorateQueueWorkerManager(ContainerBuilder $container): void {
    $definition = $container->getDefinition(InstaQueueWorkerManager::class);

    $decorationEnabled = $container->getParameter('insta_queue.decorate_queue_worker_manager');

    if ($decorationEnabled) {
      $definition->setDecoratedService('plugin.manager.queue_worker');
    }
  }

}
