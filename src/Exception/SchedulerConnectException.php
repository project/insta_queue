<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Exception;

/**
 * This Exception is thrown when a connection to the scheduler fails.
 */
class SchedulerConnectException extends \Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    parent::__construct('Could not connect to scheduler');
  }

}
