<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Exception;

/**
 * Invalid queue exception.
 *
 * This exception is thrown, when a non insta-queue
 * queue is being processed by the QueueProcessor.
 */
class InvalidQueueException extends \Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct(string $queueName) {
    parent::__construct(
      sprintf('Queue %s does not use an insta queue!', $queueName)
    );
  }

}
