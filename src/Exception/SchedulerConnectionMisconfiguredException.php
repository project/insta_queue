<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Exception;

/**
 * This Exception is thrown when the scheduler connection is misconfigured.
 */
class SchedulerConnectionMisconfiguredException extends \Exception {}
