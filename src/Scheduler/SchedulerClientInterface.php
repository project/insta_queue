<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Scheduler;

/**
 * Interface for the scheduler client.
 */
interface SchedulerClientInterface {

  /**
   * Notify the scheduler about a new item.
   */
  public function notifyQueueItemCreated(string $queueName): bool;

  /**
   * Notify the scheduler to reload configuration (e.g. queues to process).
   */
  public function notifyReload(): bool;

  /**
   * Checks connection to the scheduler.
   */
  public function checkConnection(): bool;

}
