<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Scheduler;

use Drupal\insta_queue\Exception\SchedulerConnectException;

/**
 * This class holds the pointer to the connection.
 */
class SchedulerConnection {

  /**
   * The file pointer.
   */
  protected mixed $pointer;

  /**
   * Construct new connection.
   */
  public function __construct(
    string $address,
    int $timeout,
  ) {
    $fp = stream_socket_client(address: $address, timeout: $timeout);
    if (!$fp) {
      throw new SchedulerConnectException();
    }

    $this->pointer = $fp;
  }

  /**
   * Close pointer on destruct.
   */
  public function __destruct() {
    if ($this->pointer) {
      fclose($this->pointer);
    }
  }

  /**
   * Write to the file pointer.
   */
  public function write(string $data): bool {
    return stream_socket_sendto($this->pointer, $data . PHP_EOL) !== FALSE;
  }

}
