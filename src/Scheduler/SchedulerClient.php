<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Scheduler;

use Drupal\Core\Site\Settings;
use Drupal\insta_queue\Exception\SchedulerConnectionMisconfiguredException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * Client to connect to the scheduler via TCP or Unix socket.
 */
class SchedulerClient implements SchedulerClientInterface {

  /**
   * Existing scheduler connection.
   */
  protected ?SchedulerConnection $connection = NULL;

  /**
   * Construct new InstaQueueSchedulerClient.
   */
  public function __construct(
    protected readonly Settings $settings,
    #[Autowire(service: 'logger.channel.insta_queue')]
    protected readonly LoggerInterface $logger,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function notifyQueueItemCreated(string $queueName): bool {
    $payload = 'item_created:' . $queueName;

    return $this->sendDataToScheduler($payload);
  }

  /**
   * {@inheritdoc}
   */
  public function notifyReload(): bool {
    return $this->sendDataToScheduler('reload');
  }

  /**
   * {@inheritdoc}
   */
  public function checkConnection(): bool {
    return $this->sendDataToScheduler('ping');
  }

  /**
   * Sends given data to the scheduler via TCP or Socket.
   */
  protected function sendDataToScheduler(string $data): bool {
    $connData = $this->getConnectionData();
    if (!$connData) {
      throw new SchedulerConnectionMisconfiguredException();
    }

    if (!$this->connection) {
      $this->connection = new SchedulerConnection($connData['address'], $connData['timeout']);
    }

    return $this->connection->write($data);
  }

  /**
   * Get connection data from settings.
   */
  protected function getConnectionData(): ?array {
    $data = $this->settings->get('insta_queue.scheduler_connection');
    if (!$data) {
      $this->logger->critical('Could not send queue event to scheduler: connection data not set in settings!');

      return NULL;
    }

    if (!isset($data['address'])) {
      $this->logger->critical('Could not send queue event to scheduler: scheduler connection address not set!');
      return NULL;
    }

    if (!isset($data['timeout'])) {
      $this->logger->critical('Could not send queue event to scheduler: scheduler connection timeout not set!');
      return NULL;
    }

    $address = $data['address'];
    $timeout = intval($data['timeout']);

    if (!str_starts_with($address, "tcp://") && !str_starts_with($address, "unix://")) {
      $this->logger->critical('Could not send queue event to scheduler: scheduler address must start with tcp:// or unix:// protcol scheme!');
      return NULL;
    }

    return [
      'address' => $address,
      'timeout' => $timeout,
    ];
  }

}
