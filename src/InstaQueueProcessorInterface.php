<?php

declare(strict_types=1);

namespace Drupal\insta_queue;

/**
 * Interface for a insta queue processor.
 */
interface InstaQueueProcessorInterface {

  /**
   * Process the given queue.
   *
   * Kicks off processing of the given queue.
   * Also makes sure that this queue cannot be processed
   * multiple times simultaneously.
   *
   * @param string $queueName
   *   The name of the queue to process.
   *   The queue MUST be an insta queue.
   *
   * @return QueueProcessResult
   *   The result of the processing.
   */
  public function processQueue(string $queueName): QueueProcessResult;

}
