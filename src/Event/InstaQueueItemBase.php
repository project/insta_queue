<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Base insta queue item event.
 */
abstract class InstaQueueItemBase extends Event {

  /**
   * Construct new insta queue item event.
   */
  public function __construct(
    protected string $queueName,
  ) {}

  /**
   * Get the name of the queue this event belongs to.
   */
  public function getQueueName(): string {
    return $this->queueName;
  }

}
