<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Event;

/**
 * Event for when a queue item is created.
 */
class InstaQueueItemCreatedEvent extends InstaQueueItemBase {

  /**
   * Construct new QueueItemCreatedEvent.
   */
  public function __construct(
    string $queueName,
    protected bool|int|string $itemId,
  ) {
    parent::__construct($queueName);
  }

  /**
   * Get the queue item id.
   */
  public function getItemId(): bool|int|string {
    return $this->itemId;
  }

}
