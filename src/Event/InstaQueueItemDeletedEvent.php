<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Event;

/**
 * Event for when a queue item is deleted.
 */
class InstaQueueItemDeletedEvent extends InstaQueueItemBase {}
