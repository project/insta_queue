<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Event;

/**
 * Event for when a queue item is claimed.
 */
class InstaQueueItemClaimedEvent extends InstaQueueItemBase {

  /**
   * Construct new QueueItemClaimedEvent.
   */
  public function __construct(
    string $queueName,
    protected mixed $item,
  ) {
    parent::__construct($queueName);
  }

  /**
   * Get the queue item.
   */
  public function getItem(): mixed {
    return $this->item;
  }

}
