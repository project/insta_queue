<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Event;

/**
 * Event for when a queue item is delayed.
 */
class InstaQueueItemDelayedEvent extends InstaQueueItemBase {

  /**
   * Construct new QueueItemDelayedEvent.
   */
  public function __construct(
    string $queueName,
    protected bool $success,
  ) {
    parent::__construct($queueName);
  }

  /**
   * Get whether the release was successful or not.
   */
  public function getSuccess(): bool {
    return $this->success;
  }

}
