<?php

declare(strict_types=1);

namespace Drupal\insta_queue;

/**
 * Represents the queue process result.
 *
 * Values correspond to the worker command exit code.
 */
enum QueueProcessResult: int {

  case AVAILABLE_ITEMS_PROCESSED = 0;
  case ALREADY_RUNNING = 10;
  case TIME_LIMIT_EXCEEDED = 11;
  case QUEUE_SUSPENDED = 12;

}
