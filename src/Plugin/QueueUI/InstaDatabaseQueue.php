<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Plugin\QueueUI;

use Drupal\queue_ui\Plugin\QueueUI\DatabaseQueue;

/**
 * Enables QueueUI for the InstaDatabaseQueue.
 *
 * @QueueUI(
 *   id = "insta_queue_database",
 *   class_name = "InstaDatabaseQueue"
 * )
 */
class InstaDatabaseQueue extends DatabaseQueue {}
