<?php

declare(strict_types=1);

namespace Drupal\insta_queue;

/**
 * Defines the insta_queue events.
 */
class InstaQueueEvents {

  public const QUEUE_ITEM_CREATED = 'insta_queue:queue_item_created';
  public const QUEUE_ITEM_CLAIMED = 'insta_queue:queue_item_claimed';
  public const QUEUE_ITEM_RELEASED = 'insta_queue:queue_item_released';
  public const QUEUE_ITEM_DELAYED = 'insta_queue:queue_item_delayed';
  public const QUEUE_ITEM_DELETED = 'insta_queue:queue_item_deleted';

}
