<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Queue;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManager;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

/**
 * QueueWorkerManager to support insta queues.
 */
class InstaQueueWorkerManager extends QueueWorkerManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    protected QueueFactory $queueFactory,
    #[Autowire(param: 'insta_queue.process_max_time')]
    protected int $processMaxTime,
  ) {
    parent::__construct($namespaces, $cache_backend, $module_handler);
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id): void {
    parent::processDefinition($definition, $plugin_id);

    $queue = $this->queueFactory->get($plugin_id);

    // Check if the used queue implements the InstaQueueInterface.
    // For those queues, we want to add the needed definition data
    // for the worker configuration.
    // Additionally, the cron definition is removed, which results
    // in the Drupal Cron not processing this queue.
    if ($queue instanceof InstaQueueInterface) {
      $maxProcTime = isset($definition['cron']) ? $definition['cron']['time'] : $this->processMaxTime;

      if (!isset($definition['insta_queue'])) {
        $definition['insta_queue'] = [
          'worker_max_process_time' => $maxProcTime,
        ];
      }
      elseif (!isset($definition['insta_queue']['worker_max_process_time'])) {
        $definition['insta_queue']['worker_max_process_time'] = $maxProcTime;
      }

      unset($definition['cron']);
    }
  }

}
