<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Queue;

use Drupal\Core\Database\Connection;
use Drupal\Core\Queue\DatabaseQueue;
use Drupal\insta_queue\Event\InstaQueueItemClaimedEvent;
use Drupal\insta_queue\Event\InstaQueueItemCreatedEvent;
use Drupal\insta_queue\Event\InstaQueueItemDelayedEvent;
use Drupal\insta_queue\Event\InstaQueueItemDeletedEvent;
use Drupal\insta_queue\Event\InstaQueueItemReleasedEvent;
use Drupal\insta_queue\InstaQueueEvents;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Altered DatabaseQueue to work with the insta_queu scheduler.
 *
 * Basically, this class does exactly the same as the core
 * DatabaseQueue, but additionally emits events for queue items.
 */
class InstaDatabaseQueue extends DatabaseQueue implements InstaQueueInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    $name,
    Connection $connection,
    protected EventDispatcherInterface $eventDispatcher,
  ) {
    parent::__construct($name, $connection);
  }

  /**
   * {@inheritdoc}
   */
  public function createItem($data) {
    $id = parent::createItem($data);

    $event = new InstaQueueItemCreatedEvent($this->name, $id);
    $this->eventDispatcher->dispatch($event, InstaQueueEvents::QUEUE_ITEM_CREATED);

    return $id;
  }

  /**
   * {@inheritdoc}
   */
  public function claimItem($lease_time = 30) {
    $item = parent::claimItem($lease_time);

    if (!is_bool($item)) {
      $event = new InstaQueueItemClaimedEvent($this->name, $item);
      $this->eventDispatcher->dispatch($event, InstaQueueEvents::QUEUE_ITEM_CLAIMED);
    }

    return $item;
  }

  /**
   * {@inheritdoc}
   */
  public function releaseItem($item) {
    $success = parent::releaseItem($item);

    $event = new InstaQueueItemReleasedEvent($this->name, $success);
    $this->eventDispatcher->dispatch($event, InstaQueueEvents::QUEUE_ITEM_RELEASED);

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function delayItem($item, int $delay) {
    $success = parent::delayItem($item, $delay);

    $event = new InstaQueueItemDelayedEvent($this->name, $success);
    $this->eventDispatcher->dispatch($event, InstaQueueEvents::QUEUE_ITEM_DELAYED);

    return $success;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItem($item) {
    parent::deleteItem($item);

    $event = new InstaQueueItemDeletedEvent($this->name);
    $this->eventDispatcher->dispatch($event, InstaQueueEvents::QUEUE_ITEM_DELETED);
  }

}
