<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Queue;

use Drupal\Core\Database\Connection;
use Drupal\Core\Queue\QueueDatabaseFactory;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Defines the key/value store factory for the insta queue database backend.
 */
class InstaQueueDatabaseFactory extends QueueDatabaseFactory {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Connection $connection,
    protected EventDispatcherInterface $eventDispatcher,
  ) {
    parent::__construct($connection);
  }

  /**
   * {@inheritdoc}
   */
  public function get($name) {
    return new InstaDatabaseQueue(
      $name,
      $this->connection,
      $this->eventDispatcher,
    );
  }

}
