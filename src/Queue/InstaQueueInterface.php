<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Queue;

/**
 * Insta queue interface.
 *
 * Classes implementing this interface indicate that
 * the queue is managed via the insta queue scheduler.
 *
 * Queues implementing this interface will not be
 * processed via cron!
 *
 * @see \Drupal\Core\Cron::processQueues
 */
interface InstaQueueInterface {}
