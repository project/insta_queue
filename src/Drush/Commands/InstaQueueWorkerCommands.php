<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Drush\Commands;

use Drupal\insta_queue\Exception\InvalidQueueException;
use Drupal\insta_queue\InstaQueueProcessor;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements insta queue worker commands.
 */
final class InstaQueueWorkerCommands extends DrushCommands {

  /**
   * Constructs an InstaQueueWorkerCommands object.
   */
  public function __construct(
    private readonly InstaQueueProcessor $instaQueueProcessor,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('insta_queue.processor'),
    );
  }

  /**
   * Command to process a given queue.
   *
   * Only works for queues using an insta_queue queue implementation.
   * See readme for more info.
   */
  #[CLI\Command(name: 'insta_queue:process-queue', aliases: ['iqp'])]
  #[CLI\Argument(name: 'queue', description: 'Name of the queue to process.')]
  #[CLI\Usage(name: 'insta_queue:process-queue my-queue', description: 'Processes items in "my-queue". Note, that my-queue must use a insta_queue queue implementation!')]
  public function processQueue(string $queue): int {
    try {
      $result = $this->instaQueueProcessor->processQueue($queue);
      return $result->value;
    }
    catch (InvalidQueueException $e) {
      $this->yell($e->getMessage(), 40, 'red');

      return 1;
    }

  }

}
