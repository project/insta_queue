<?php

declare(strict_types=1);

namespace Drupal\insta_queue\Drush\Commands;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\insta_queue\Queue\InstaQueueInterface;
use Drush\Attributes as CLI;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements insta queue scheduler commands.
 */
final class InstaQueueSchedulerCommands extends DrushCommands {

  /**
   * Constructs an InstaQueueWorkerCommands object.
   */
  public function __construct(
    private readonly QueueWorkerManagerInterface $queueWorkerManager,
    private readonly QueueFactory $queueFactory,
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.queue_worker'),
      $container->get('queue'),
    );
  }

  /**
   * Command to get all queues that implement InstaQueueInterface as json array.
   */
  #[CLI\Command(name: 'insta_queue:get-insta-queues')]
  public function getInstaQueues(): void {
    $instaQueues = [];

    foreach (array_keys($this->queueWorkerManager->getDefinitions()) as $name) {
      $queue = $this->queueFactory->get($name);

      if ($queue instanceof InstaQueueInterface) {
        $instaQueues[] = $name;
      }
    }

    $this->output()->write(Json::encode($instaQueues));
  }

}
